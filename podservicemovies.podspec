Pod::Spec.new do |spec|

  spec.name         = "podservicemovies"
  spec.version      = "1.0.0"
  spec.summary      = "Its a framework servicesmovies"
  spec.description  = "I dont have idea but i can try improve my framework"

  spec.homepage     = "https://gitlab.com/lissb88/podservicemovies"
  #

  spec.license      = "MIT"

  spec.author             = { "Liset Martínez" => "lissb88@gmail.com" }
  spec.platform     = :ios, "13.0"

  spec.source       = { :git => "https://gitlab.com/lissb88/podservicemovies.git", :tag => spec.version.to_s }
#spec.source       = { :git => "https://gitlab.com/joaquin13/servicesmovies.git", :tag => "#{spec.version}" }

  spec.source_files  = "podservicemovies/**/*.{swift}"
  spec.swift_versions = "5.0"

  # spec.requires_arc = true

end
