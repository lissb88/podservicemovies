//
//  movieModel.swift
//  PruebaGonet
//
//  Created by Liss SM on 28/11/21.
//

import Foundation


public struct MoviesUrlService:Codable {
    
    
    public var results:[Results]?
    public var items:[Results]?
    /*
    init(results:[Results]?,items:[Results]?){
        self.results = results
        self.items = items
    }*/
    
    enum CodingKeys: String, CodingKey{
        case results = "results"
        case items = "items"
    }
    
    public struct Results:Codable {
        
        public var popularity: Double = 0.0
        public var vote_count: Double = 0.0
        public var poster_path: String = ""
        public var original_language: String = ""
        public var original_title: String = ""
        public var title: String = ""
        public var vote_average: Double = 0.0
        public var overview: String = ""
        public var release_date: String = ""
        
        init( popularity: Double,
         vote_count: Double ,
         poster_path: String,
         original_language: String,
         original_title: String ,
         title: String  ,
         vote_average: Double ,
         overview: String ,
         release_date: String){
            self.popularity = popularity
            self.vote_count = vote_count
            self.poster_path = poster_path
            self.original_title = original_title
            self.original_language = original_language
            self.title = title
            self.vote_average = vote_average
            self.overview = overview
            self.release_date = release_date
        }
        
        enum CodingKeys: String, CodingKey{
            case popularity = "popularity"
            case vote_count = "vote_count"
            case poster_path = "poster_path"
            case original_title = "original_title"
            case original_language = "original_language"
            case title = "title"
            case vote_average = "vote_average"
            case overview = "overview"
            case release_date = "release_date"
        }
    }
}
public struct TVUrlService:Codable {
    
    
    public var results:[Results]?
    
    enum CodingKeys: String, CodingKey{
        case results = "results"
    }
    
    public struct Results:Codable {
        
        public var popularity: Double = 0.0
        public var vote_count: Double = 0.0
        public var poster_path: String = ""
        public var original_language: String = ""
        public var original_name: String = ""
        public var name: String = ""
        public var vote_average: Double = 0.0
        public var overview: String = ""
        public var first_air_date: String = ""
        
        init( popularity: Double,
         vote_count: Double ,
         poster_path: String,
         original_language: String,
         original_name: String ,
         name: String  ,
         vote_average: Double ,
         overview: String ,
         first_air_date: String){
            self.popularity = popularity
            self.vote_count = vote_count
            self.poster_path = poster_path
            self.original_name = original_name
            self.original_language = original_language
            self.name = name
            self.vote_average = vote_average
            self.overview = overview
            self.first_air_date = first_air_date
        }
        
        enum CodingKeys: String, CodingKey{
            case popularity = "popularity"
            case vote_count = "vote_count"
            case poster_path = "poster_path"
            case original_language = "original_language"
            case original_name = "original_name"
            case name = "name"
            case vote_average = "vote_average"
            case overview = "overview"
            case first_air_date = "first_air_date"
        }
    }
}

