//
//  GetExternalData.swift
//  podservicemovies
//
//  Created by Liss SM on 29/11/21.
//


public protocol ExternalServiceProtocol {
    func returnData(Rated: MoviesUrlService, Recomendaciones: MoviesUrlService, Favoritas: MoviesUrlService,TVRated: TVUrlService, TVRecomendaciones: TVUrlService, TVFavoritas: TVUrlService)
}

import Foundation

public class GetExternalData {
    
    
    var listFavoriteMovies: MoviesUrlService?
    var listRecommended : MoviesUrlService?
    var listRated: MoviesUrlService?
    var listFavoriteTV: TVUrlService?
    var listRecommendedTV : TVUrlService?
    var listRatedTV: TVUrlService?
    var delegateService: ExternalServiceProtocol?
    
    public init(delegate: AnyObject){
        self.delegateService = delegate as? ExternalServiceProtocol
    }
    
    public func externalGetData() {
        
        self.getService1()
    }
    
    
    func getService1() {
        let session = URLSession.shared
        var request = URLRequest(url: URL(string: "https://api.themoviedb.org/3/list/15570?api_key=57e3d9e923f56764865af1e97c15bcc9")!)
        request.httpMethod = "GET"
        
        request.setValue("application/json; charset=utf8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/x-www-form-urlencoded; charset=utf8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("no-cache", forHTTPHeaderField: "cache-control")

        
        
        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
            print(response!)
            
            guard let data = data, error == nil, let respuesta = response  as? HTTPURLResponse else{
                print("Error de conexion ")
                return
            }
            
            if respuesta.statusCode == 200{
                print("Respuesta \(data)")
                do {
                    let decoder = JSONDecoder()
                    self.listFavoriteMovies = try decoder.decode(MoviesUrlService.self, from: data)
                    self.getService2()
                } catch {
                    print("error \(error.localizedDescription)")
                }
            }else{
                print(" error  \(respuesta.statusCode)")
            }
        })

        task.resume()
    }
    
    func getService2(){
        let session = URLSession.shared
        var request = URLRequest(url: URL(string: "https://api.themoviedb.org/3/movie/popular?api_key=57e3d9e923f56764865af1e97c15bcc9")!)
        request.httpMethod = "GET"
        
        request.setValue("application/json; charset=utf8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/x-www-form-urlencoded; charset=utf8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("no-cache", forHTTPHeaderField: "cache-control")

        
        
        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
            print(response!)
            
            guard let data = data, error == nil, let respuesta = response  as? HTTPURLResponse else{
                print("Error de conexion ")
                return
            }
            
            if respuesta.statusCode == 200{
                
                print("Respuesta \(data)")
                
                do {
                    
                    let decoder = JSONDecoder()
                    self.listRecommended = try decoder.decode(MoviesUrlService.self, from: data)
                    self.getService3()
                    
                } catch {
                    print("error \(error.localizedDescription)")
                }
            }else{
                print(" error  \(respuesta.statusCode)")
            }
        })

        task.resume()
    }
    
    func getService3(){
        let session = URLSession.shared
        var request = URLRequest(url: URL(string: "https://api.themoviedb.org/3/movie/top_rated?api_key=57e3d9e923f56764865af1e97c15bcc9")!)
        request.httpMethod = "GET"
        
        request.setValue("application/json; charset=utf8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/x-www-form-urlencoded; charset=utf8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("no-cache", forHTTPHeaderField: "cache-control")

        
        
        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
            print(response!)
            
            guard let data = data, error == nil, let respuesta = response  as? HTTPURLResponse else{
                print("Error de conexion ")
                return
            }
            
            if respuesta.statusCode == 200{
                
                print("Respuesta \(data)")
                do {
                    let decoder = JSONDecoder()
                    self.listRated = try decoder.decode(MoviesUrlService.self, from: data)
//                    self.remoteRequestHandler?.callBackData(Rated: rat, Recomendaciones: rec, Favoritas: fav)
                    self.getService4()
                    
                } catch {
                    print("error \(error.localizedDescription)")
                }
            }else{
                print(" error  \(respuesta.statusCode)")
            }
        })

        task.resume()
    }
    func getService4(){
            let session = URLSession.shared
            var request = URLRequest(url: URL(string: "https://api.themoviedb.org/3/tv/top_rated?api_key=57e3d9e923f56764865af1e97c15bcc9")!)
            request.httpMethod = "GET"
            
            request.setValue("application/json; charset=utf8", forHTTPHeaderField: "Content-Type")
            request.setValue("application/x-www-form-urlencoded; charset=utf8", forHTTPHeaderField: "Content-Type")
            request.setValue("application/json", forHTTPHeaderField: "Accept")
            request.setValue("no-cache", forHTTPHeaderField: "cache-control")

            
            
            let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
                print(response!)
                
                guard let data = data, error == nil, let respuesta = response  as? HTTPURLResponse else{
                    print("Error de conexion ")
                    return
                }
                
                if respuesta.statusCode == 200{
                    
                    print("Respuesta \(data)")
                    
                    do {
                        
                        let decoder = JSONDecoder()
                        self.listFavoriteTV = try decoder.decode(TVUrlService.self, from: data)
                        self.getService5()
                        
                    } catch {
                        print("error \(error.localizedDescription)")
                    }
                }else{
                    print(" error  \(respuesta.statusCode)")
                }
            })

            task.resume()
        }
    func getService5(){
            let session = URLSession.shared
            var request = URLRequest(url: URL(string: "https://api.themoviedb.org/3/tv/popular?api_key=57e3d9e923f56764865af1e97c15bcc9")!)
            request.httpMethod = "GET"
            
            request.setValue("application/json; charset=utf8", forHTTPHeaderField: "Content-Type")
            request.setValue("application/x-www-form-urlencoded; charset=utf8", forHTTPHeaderField: "Content-Type")
            request.setValue("application/json", forHTTPHeaderField: "Accept")
            request.setValue("no-cache", forHTTPHeaderField: "cache-control")

            
            
            let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
                print(response!)
                
                guard let data = data, error == nil, let respuesta = response  as? HTTPURLResponse else{
                    print("Error de conexion ")
                    return
                }
                
                if respuesta.statusCode == 200{
                    
                    print("Respuesta \(data)")
                    
                    do {
                        
                        let decoder = JSONDecoder()
                        self.listRecommendedTV = try decoder.decode(TVUrlService.self, from: data)
                        self.getService6()
                        
                    } catch {
                        print("error \(error.localizedDescription)")
                    }
                }else{
                    print(" error  \(respuesta.statusCode)")
                }
            })

            task.resume()
        }
    func getService6(){
        let session = URLSession.shared
        var request = URLRequest(url: URL(string: "https://api.themoviedb.org/3/tv/top_rated?api_key=57e3d9e923f56764865af1e97c15bcc9")!)
        request.httpMethod = "GET"
        
        request.setValue("application/json; charset=utf8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/x-www-form-urlencoded; charset=utf8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("no-cache", forHTTPHeaderField: "cache-control")

        
        
        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
            print(response!)
            
            guard let data = data, error == nil, let respuesta = response  as? HTTPURLResponse else{
                print("Error de conexion ")
                return
            }
            
            if respuesta.statusCode == 200{
                
                print("Respuesta \(data)")
                
                do {
                    
                    let decoder = JSONDecoder()
                    self.listRatedTV = try decoder.decode(TVUrlService.self, from: data)
                    print("Ya consumio los 6 servicios")
                    guard let fav = self.listFavoriteMovies, let rec = self.listRecommended, let rat = self.listRated, let tvfav = self.listFavoriteTV, let tvrec = self.listRecommendedTV, let tvrat = self.listRatedTV  else {return}
                    self.delegateService?.returnData(Rated: rat, Recomendaciones: rec, Favoritas: fav, TVRated: tvrat, TVRecomendaciones: tvrec, TVFavoritas: tvfav)
                    return
                    
                } catch {
                    print("error \(error.localizedDescription)")
                }
            }else{
                print(" error  \(respuesta.statusCode)")
            }
        })

        task.resume()
    }
    
}
